#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include "my_data.h"
#include "my_stack.h"
#include "my_interface.h"

#pragma warning (disable : 4996)

static char *strtab[] =
{
	"0 - push",			//INTERF_PUSH
	"1 - pop",			//INTERF_POP
	"2 - clear",		//INTERF_CLEAR
	"3 - finish"        //INTERF_STOP
};

void menu()
{
	size_t it;
	for (it = 0; it<INTERF_TOT; ++it)
	{
		printf("%s\n", strtab[it]);
	}
}

void push()
{
	char name[512], lastname[512];
	size_t year;
	printf("name, lastname, year\n");
	scanf("%s", name);
	scanf("%s", lastname);
	scanf("%d", &year);

	void *pdat = MY_DATA_Push(name, lastname, year);
	if (!MY_STACK_Push(pdat))
		printf("push error\n");
}

void pop()
{
	MY_STACK tmp = MY_STACK_Pop();
	MY_DATA_Print(tmp.pData);
	MY_DATA_Free(tmp.pData);
}

void clear()
{
	MY_STACK_Free();
}