#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include "my_data.h"
#include "string.h"

#pragma warning (disable : 4996)

void *MY_DATA_Init(char *nname, char *llastname, size_t year)
/*==========================================================
Alokujemy pamiec dla obiektu MY_DATA i wypelniamy dane
===========================================================*/
{
	MY_DATA *pdat = (MY_DATA *)malloc(sizeof(MY_DATA));
	if (pdat)
	{
		strcpy(pdat->lastname, llastname);
		strcpy(pdat->name, nname);
		pdat->year = year;
	}
	return (void *)(pdat);
}


void MY_DATA_Free(void *ptr)
/*==========================================================
Zwolniamy pamiec
===========================================================*/
{
	if (ptr)
		free(ptr);
}

void * MY_DATA_Push(char *name, char *lastname, size_t year)
{
	return MY_DATA_Init(name, lastname, year);
}

void MY_DATA_Print(void *ptr)
{
	MY_DATA *p = (MY_DATA *)ptr;
	if (p)
	{
		printf("name    : %s\n", p->name);
		printf("lastname: %s\n", p->lastname);
		printf("year    : %d\n", p->year);
	}
}