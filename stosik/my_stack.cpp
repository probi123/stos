#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include "my_data.h"
#include "my_stack.h"

static MY_STACK *first = NULL;  //wskaznik do pierwszego elementu w kolejce
static MY_STACK *last = NULL;  //wskaznik do ostatniego elementu w kolejce

void MY_STACK_Init()
{
	first = last = NULL;
}

void MY_STACK_Free()
{
	//Zwalniamy cala kolejke
	//Ustaliamy wskaznik p do first
	MY_STACK *p = first, *ptmp = NULL;

	//pobieramy po kolejce obiekty
	while (p)
	{
		//Zwalniamy dane dla biezacego elementu kolejki, do ktorego wskazuje wskaznik p
		if (p->pData)
			MY_DATA_Free(p->pData);
		//Przypisujemy adres biezacego elementu do pomocniczej zmiennej ptmp
		ptmp = p;

		//przestawiamy wskaznik p do nastepnego elementu kolejki
		p = p->next;

		//zwalniamy biezacy element kolejki
		free(ptmp);
	}

	last = first = NULL;
}

MY_STACK * MY_STACK_Push(void *pdat)
{
	//alokujemy pamiec dla nowego elementu kolejki
	MY_STACK *current = (MY_STACK *)malloc(sizeof(MY_STACK));
	if (!current)
		return NULL;

	//Teraz last wskazuje do przedostatniego elementu, ktory
	//ma next = NULL, poniewaz do dokladania biezacego elementu on byl ostatnim.
	//Sprawdzamy, czy biezacy element nie jest pierwszym w kolejce.
	if (last)
		last->next = current;	//last wskazuje do elementu, ktory byl ostatni, 
								//jednak okazal sie przedostatni 

								//Jest to element ostatni - next = NULL, last = current;
	current->next = NULL;
	last = current;

	//Jesli to jest pierwszy element kolejki, first powinien wskazywac na niego
	if (!first)
		first = current;

	//Ustawiamy pData
	current->pData = pdat;

	return last;
}

MY_STACK MY_STACK_Pop()
{
	MY_STACK rv;
	if (!first)
	{
		//kolejka jest pusta
		rv.pData = NULL;
		rv.next = NULL;
	}
	else
	{
		//Pobieramy pierwszy element
		//Nastepny element powinien byc first. Pobieramy jego adres.
		MY_STACK *next = first->next;

		//Pobieramy dane dla elementu first.
		rv.pData = first->pData;

		//Nie jest to obowjazkowe
		rv.next = NULL;

		//zwalniamy pamiec dla elementu first
		free(first);

		//przestawiamy first do nastepnego elementu
		first = next;
		if (first == NULL)
			last = NULL;    //kolejka iest pusta
	}

	return rv;
}