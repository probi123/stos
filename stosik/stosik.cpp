// stosik.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include "my_interface.h"

#pragma warning (disable : 4996)

int main(int argc, _TCHAR* argv[])
{
	size_t op = 0;
	while (op >= INTERF_PUSH && op <= INTERF_STOP)
	{
		menu();
		scanf("%d", &op);
		switch (op)
		{
		case INTERF_PUSH: push();
			break;
		case INTERF_POP: pop();
			break;
		case INTERF_CLEAR: clear();
			break;
		case INTERF_STOP: clear();
			return 0;
		default:
			printf("nieuznawany kod operacji\n");
		};
	}


	return 0;
}
