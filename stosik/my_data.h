#ifndef MY_DATA_ADFGRETW__H
#define MY_DATA_ADFGRETW__H

struct MY_DATA
{
	char name[512];
	char lastname[512];
	size_t year;
};

void *MY_DATA_Init(char *nname, char *llastname, size_t year);
void MY_DATA_Free(void *ptr);
void * MY_DATA_Push(char *name, char *lastname, size_t year);
void MY_DATA_Print(void *ptr);

#endif