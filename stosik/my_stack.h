#ifndef MY_STACK___H
#define MY_STACK___H

struct MY_STACK
{
	void *pData;    //wskaznik do danych typu void *
	MY_STACK *next; //wskaznik do nastepnego elementu kolejki	
};

void MY_STACK_Init();
void MY_STACK_Free();
MY_STACK * MY_STACK_Push(void *pdat);
MY_STACK MY_STACK_Pop();

#endif